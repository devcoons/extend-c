#include "extend.h"

int set_affinity(uint32_t cores, pthread_t thread) 
{
   sysconf(_SC_NPROCESSORS_ONLN);
   int i;
   cpu_set_t cpuset;
   CPU_ZERO(&cpuset);
   for(i=0;i<32;i++)
	   if( ( cores & ((uint32_t)1 ) <<i ) !=0) CPU_SET(i,&cpuset);
   return pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset);
}

int get_affinity(pthread_t thread)
{
        int j, cores=0;
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        pthread_getaffinity_np(thread,sizeof(cpu_set_t),&cpuset);
        for (j = 0; j < 32; j++)
               if (CPU_ISSET(j, &cpuset))
               cores+=(j+1);
        return cores;
}
