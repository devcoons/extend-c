#ifndef _EXTEND_H
	#define _EXTEND_H
	#ifdef __cplusplus
		extern "C" {
	#endif

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <stdarg.h>
	#include <ctype.h>
	#include <netdb.h>
	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <unistd.h>
	#ifndef _GNU_SOURCE
 	#define _GNU_SOURCE 
	#endif
	#ifndef __USE_GNU
	#define __USE_GNU
	#endif 
	#include <sched.h>
	#include <errno.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <sched.h>
	#include <cpuset.h>
	#include <bitmask.h>
	#include <pthread.h>
	#include <math.h>
	#define strlens(s) (s==NULL?0:strlen(s))

	typedef char  bitfield;
	
	typedef struct struct_args
	{
		char * 	_arg;
		int 	_level;
		int 	_data;
		void(*_function)(char *);
	}struct_args;

	extern char * str_trim		(char *);
	extern char * str_ltrim		(char *);
	extern char * str_rtrim		(char *);	
	extern char * str_merge		(int, ...);
	extern char * str_reverse	(char *);
	extern char * str_uppercase	(char *);
	extern char * str_lowercase	(char *);	
	extern char * command_exec	(char *);
	extern char * file_read		(char *);
	extern char ** str_split	(char *,char);	
	extern char * num_to_str	(double);
	extern int str_contains		(char *, char *);
	extern int file_write		(char *, char *);
	extern int file_exists		(char *);
	extern int file_append		(char *, char *);
	extern int file_create		(char *);
	extern int file_delete		(char *);
	extern int net_available	(void);

	extern void bitfield_reverse	(bitfield *);
	extern void bitfield_print	(bitfield *);
	extern void bitfield_flip	(bitfield *);
	
	extern void bitfield_bit_flip		(bitfield *, int);
	extern void bitfield_bit_change		(bitfield *, int, int);
	extern char bitfield_bit_retrieve	(bitfield *, int);
	
	extern void args_parsing(int, char **, struct struct_args *);
	
	extern int set_affinity(uint32_t, pthread_t);
	extern int get_affinity(pthread_t);

	
	double power (double , int ) __attribute__((visibility ("hidden")));

#ifdef __cplusplus
}
#endif
#endif
